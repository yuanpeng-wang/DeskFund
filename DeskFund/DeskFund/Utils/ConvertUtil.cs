﻿/********************************\
 * C# 快速类型转换工具
 * 版本 1.1
\********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeskFund
{
    public class ConvertUtil
    {
        /// <summary>
        /// 快速转换double，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static double ToDouble(object value, double defaultValue)
        {
            try
            {
                defaultValue = Convert.ToDouble(value); 
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速转换double，转换失败将返回 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(object value)
        {
            return ToDouble(value, 0);
        }
        /// <summary>
        /// 快速转换decimal，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static decimal ToDecimal(object value, decimal defaultValue)
        {
            try
            {
                defaultValue = Convert.ToDecimal(value);
                defaultValue = Math.Round(defaultValue,2);
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速转换double，转换失败将返回 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ToDecimal(object value)
        {
            return ToDecimal(value, 0);
        }
        /// <summary>
        /// 快速转换数字，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(object value, int defaultValue)
        {
            try
            {
                defaultValue = Convert.ToInt32(value);
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速转换数字，转换失败将返回 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt32(object value)
        {
            return ToInt32(value, 0);
        }

        /// <summary>
        /// 快速转换数字Int64，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToInt64(object value, long defaultValue)
        {
            try
            {
                defaultValue = Convert.ToInt64(value);
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速转换数字Int64，转换失败将返回 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToInt64(object value)
        {
            return ToInt64(value, 0);
        }

        /// <summary>
        /// 快速转换布尔，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool ToBool(object value, bool defaultValue)
        {
            try
            {
                if (value != null && value.ToString().ToLower().Contains("true"))
                {
                    defaultValue = true;
                    return defaultValue;
                }
                if (value != null && value.ToString().ToLower().Contains("false"))
                {
                    defaultValue = false;
                    return defaultValue;
                }
                if (value == null || value.ToString() == "")
                {
                    defaultValue = false;
                    return defaultValue;
                }
                if (int.Parse(value.ToString())>1)
                {
                    defaultValue = true;
                    return defaultValue;
                }
                else
                {
                    defaultValue = false;
                    return defaultValue;
                }
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速对象转换布尔，转换失败将返回 false
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ToBool(object value)
        {
            return ToBool(value, false);
        }

        /// <summary>
        /// 数值对象转换布尔(非0为true)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ToBoolFromInt(int value)
        {
            return value != 0;
        }

        /// <summary>
        /// 快速转换日期，转换失败将返回默认值
        /// <param name="defaultValue"></param>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(object value, DateTime defaultValue)
        {
            try
            {
                defaultValue = Convert.ToDateTime(value);
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 快速转换日期，转换失败将返回当前时间
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(object value)
        {
            return ToDateTime(value, DateTime.Now);
        }

        /// <summary>
        /// 快速转换成十六进制，转换失败将返回 "0x0"
        /// </summary>
        /// <param name="value">整数</param>
        /// <returns></returns>
        public static string ToHex(object value)
        {
            string result = "0x0";
            try
            {
                result = "0x" + Convert.ToString(Convert.ToInt32(value), 16).ToUpper();
            }
            catch { }
            return result;
        }

        /// <summary>
        /// 快速转换IP地址，转换失败将返回默认值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ToIpAddress(object value, string defaultValue)
        {
            try
            {
                defaultValue = System.Net.IPAddress.Parse(value.ToString()).ToString();
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 格式化字串，当对象为空时使用默认字串，含SQL敏感字符替换
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ToString(object obj, string defaultValue)
        {
            try
            {
                defaultValue = obj.ToString().Replace("'", "’");
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 格式化字串，当字串为空时使用空白字串，含SQL敏感字符替换
        /// </summary>
        /// <param name="str"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ToString(object obj)
        {
            return ToString(obj, "");
        }
    }
}
